# Trabalho de Sistemas Distribuídos com base no artigo ` Turbinando suas aplicações com Multithreading`

## Aluno: Leanderson Coelho

## Sobre 
  - O trabalho fala sobre a estratégia de Thread Produtor/Consumidor onde cada exemplo trata sobre probelmas e soluções dessa estratégia.
>>
  - A primeira estratégia abordada e implementada é sobre Thread Produtora e Consumidora sem sincronismo, onde nada garante que o consumidor só ira recuperar os valores do buffer depois que o produtor inserir e que o produtor apenas insira dados no buffer quando existir espaço para isso. A minha implementação está disponível no pacote `com.sd.multithreading.standard`.
>>
   - A segunda estratégia abordada já traz sincronismo com a implemetação de `Lock` e `Conditional` onde as Threads ficam em espera dependendo dos `Lock` ativos e podem retornar à atividade quando sua `Conditional` de espera for satisfeita. A minha implementação está disponível no pacote `com.sd.multithreading.withLock`.
>>
   - A terceira estrátegia utilizada é sincronismo e buffer circular, onde o buffer consegue armazenar mais de um valor fazendo o que a espera da Thread Produtora/Consumidora em certos casos seja menor. A minha implementação está disponível no pacote `com.sd.multithreading.circularArea`.
>>
   - A quarta e ultima implementação já traz a utilização da classe Java `ArrayBlockingQueue` que faz toda a implementação de `Lock` da Thread e ainda traz o conceito de mais de um valor armazenado no `Buffer`. A minha implementação está disponível no pacote `com.sd.multithreading.withArrayBlockingQueue`.

## Execução 
 - Cada pacote contém um README com as informações para execução do exemplo.

## Dominínio de exemplo

O meu exemplo é algo semelhante a uma fila de banco para atendimento aos clientes, ou qualquer outra fila de atendimento, onde o próximo ticket é chamdo e o cliente com o ticket referente deve ser atendido.