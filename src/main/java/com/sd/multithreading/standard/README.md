# Execução

Com todo ambiente Java configurado execute os seguintes passos:

**Na raiz de todos os pacotes, execute:**

Crie um diretório de saída para os arquivos `.java` que serão compilados. Exemplo `out`.

> mkdir out

Compile todos os arquivos do pacote e informe que o resultado é inserido na pasta `out`.

> javac **standard**/*.java -d **out**

Agora execute o `Main`

> cd out
>
> java com.sd.multithreading.**standard**.StandardExample