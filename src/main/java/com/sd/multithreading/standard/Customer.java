package com.sd.multithreading.standard;

public class Customer implements Buffer {

    private int ticket;

    public Customer(int ticket) {
        this.ticket = ticket;
    }


    @Override
    public int get() {
//        System.out.println("Consumer read \t\t" + ticket);
        return ticket;
    }

    @Override
    public void set(int value) {
//        System.out.println("Producer write " + ticket);
        ticket = value;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "ticket=" + ticket +
                '}';
    }
}
