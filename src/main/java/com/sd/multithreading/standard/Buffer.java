package com.sd.multithreading.standard;

public interface Buffer {

    public int get();

    public void set(int value);
}
