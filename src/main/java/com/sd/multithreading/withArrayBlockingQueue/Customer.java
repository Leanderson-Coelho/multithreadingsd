package com.sd.multithreading.withArrayBlockingQueue;


import java.util.concurrent.ArrayBlockingQueue;

public class Customer implements Buffer {

    // implementação de um Lock com fila para o tipo int
    private ArrayBlockingQueue<Integer> buffer;


    public Customer() {
        buffer = new ArrayBlockingQueue<Integer>(3);
    }


    @Override
    public int get() {
        int readValue = 0;
        try {
            // nesse momento a thread já é bloqueado automaticamente caso não tenha nenhum elemento para ser consumido, caso contrario consome continua
            readValue = buffer.take();
            System.out.println("Consumer read: " + readValue + " buffers size: " + buffer.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return readValue;
    }

    @Override
    public void set(int value) {
        try {
            // nesse momento a thread já é bloqueada caso todo o buffer esteja cheio, caso contrario continua
            buffer.put(value);
            System.out.println("Producer write: " + value + " buffers size: " + buffer.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
