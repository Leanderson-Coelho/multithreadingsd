package com.sd.multithreading.withArrayBlockingQueue;

public interface Buffer {

    public int get();

    public void set(int value);
}
