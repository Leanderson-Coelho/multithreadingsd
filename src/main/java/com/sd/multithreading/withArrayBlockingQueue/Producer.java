package com.sd.multithreading.withArrayBlockingQueue;

import java.util.Random;

public class Producer implements Runnable{

    // gerador de valores aleatórios
    private final Random random = new Random();

    // Buffer compartilhado
    private final Buffer buffer;

    public Producer(Buffer buffer) {
        this.buffer = buffer;
    }

    @Override
    public void run() {
        for (int count = 0; count < 9; count ++) {
            try {
                Thread.sleep(random.nextInt(3000));
                buffer.set(count);
                System.out.println("Next customer: " + count);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("End of Producer");
    }
}
