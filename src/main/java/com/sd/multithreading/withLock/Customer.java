package com.sd.multithreading.withLock;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Customer implements Buffer {

    private int ticket;

    // Lock que define se o Buffer pode ser utilizado ou nao
    private Lock accessLock = new ReentrantLock();

    // condicional que informa se o produtor pode gravar ou nao
    private Condition canWrite = accessLock.newCondition();
    // condicional que infroma se o consumidor pode ler
    private Condition canRead = accessLock.newCondition();

    // usado quando o produtor não pode/pode gravar
    // usado quando o consumidor não pode/pode ler
    private boolean occupied = false;


    public Customer(int ticket) {
        this.ticket = ticket;
    }


    @Override
    public int get() {
        int readValue = 0;

        accessLock.lock();
        try {
            while (!occupied) {
                System.out.println("Consumer try read");
                canRead.await();
                showState("Empty Buffer, Consumer wait");
            }
            occupied = false;
            readValue = ticket;
            showState("Consumer read: " + readValue);
            canWrite.signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            accessLock.unlock();
        }

        return readValue;
    }

    @Override
    public void set(int value) {
        accessLock.lock();
        try {
            while (occupied) {
                System.out.println("Producer try write");
                showState("full Buffer, Producer wait");
                canWrite.await();
            }
            ticket = value;
            occupied = true;
            showState("Producer write: " + ticket);
            canRead.signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            accessLock.unlock();
        }
    }

    public void showState(String operation) {
        System.out.printf("%-40s%d\t\t%b\n\n", operation, ticket, occupied);
    }

    @Override
    public String toString() {
        return "Customer{" +
                "ticket=" + ticket +
                '}';
    }
}
