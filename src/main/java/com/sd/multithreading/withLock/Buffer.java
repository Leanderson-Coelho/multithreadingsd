package com.sd.multithreading.withLock;

public interface Buffer {

    public int get();

    public void set(int value);
}
