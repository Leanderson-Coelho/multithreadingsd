package com.sd.multithreading.withLock;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class withLockExample {
    public static void main(String[] args) {
        System.out.println("com.sd.multithreading.withLock");

        ExecutorService executorService = Executors.newFixedThreadPool(2);

        Buffer sharedData = new Customer(-1);

        try {
            executorService.execute(new Producer(sharedData));
            executorService.execute(new Consumer(sharedData));
        } catch (Exception e) {
            e.printStackTrace();
        }
        executorService.shutdown();

    }
}
