package com.sd.multithreading.circularArea;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Customer implements Buffer {

    // buffer circular
    private int[] buffer = {-1,-1,-1};

    private int occupiedBuffer = 0;
    private int writeIndex = 0;
    private int readIndex = 0;

    // Lock que define se o Buffer pode ser utilizado ou nao
    private Lock accessLock = new ReentrantLock();

    // condicional que informa se o produtor pode gravar ou nao
    private Condition canWrite = accessLock.newCondition();
    // condicional que infroma se o consumidor pode ler
    private Condition canRead = accessLock.newCondition();

    // usado quando o produtor não pode/pode gravar
    // usado quando o consumidor não pode/pode ler
    private boolean occupied = false;


    public Customer() {}


    @Override
    public int get() {
        int readValue = 0;

        accessLock.lock();
        try {
            while (occupiedBuffer == 0) {
                System.out.println("Consumer try read");
                showState("All Buffers Empty , Consumer wait");
                canRead.await();
            }
            readValue = buffer[readIndex];
            readIndex = (readIndex + 1) % buffer.length;
            occupiedBuffer--;
            showState("Consumer read: " + readValue);
            canWrite.signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            accessLock.unlock();
        }
        return readValue;
    }

    @Override
    public void set(int value) {
        accessLock.lock();
        try {
            while (occupiedBuffer == buffer.length) {
                System.out.println("All Buffers full, producer wait");
                canWrite.await();
            }
            buffer[writeIndex] = value;
            writeIndex = (writeIndex + 1) % buffer.length;
            occupiedBuffer++;
            showState("Producer write: " + value);
            canRead.signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            accessLock.unlock();
        }
    }

    public void showState(String operation) {
        System.out.printf(operation + " occupied: " + occupied + "\n");
    }
}
