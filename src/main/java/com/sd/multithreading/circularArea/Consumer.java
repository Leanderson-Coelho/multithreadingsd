package com.sd.multithreading.circularArea;

import java.util.Random;

public class Consumer implements Runnable {

    // gerador de valores aleatórios
    private final Random random = new Random();


    // Buffer compartilhado
    private final Buffer buffer;

    public Consumer(Buffer buffer) {
        this.buffer = buffer;
    }

    @Override
    public void run() {
        for (int count = 0; count < 9; count ++) {
            try {
                Thread.sleep(random.nextInt(3000));
                System.out.println("Serving customer with ticket: " + buffer.get());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
