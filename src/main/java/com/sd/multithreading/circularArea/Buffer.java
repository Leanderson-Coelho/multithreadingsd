package com.sd.multithreading.circularArea;

public interface Buffer {

    public int get();

    public void set(int value);
}
