package com.sd.multithreading.circularArea;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class circularAreaExample {
    public static void main(String[] args) {
        System.out.println("com.sd.multithreading.circularArea");

        ExecutorService executorService = Executors.newFixedThreadPool(2);

        Buffer sharedData = new Customer();

        try {
            executorService.execute(new Producer(sharedData));
            executorService.execute(new Consumer(sharedData));
        } catch (Exception e) {
            e.printStackTrace();
        }
        executorService.shutdown();

    }
}
